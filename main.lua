require 'text'
require 'procgen'
require 'behaviour'
require 'namegen'

function love.load()

	--variables
	i,j,k,l,N = 0,0,0,0,0
	Own_X, Own_Y = 544,384
	MeOnField_X, MeOnField_Y = 1, 1

	IngameChecker = 0
	TimeChecker = 0
	Seconds = 0
	RoundCounter = 0
	
	LoggedAmount = 0
	TerminalsVisited = 0
	TerminalCondition = 0
	HasDynamic = false
	DynamicDir = 1
	Dynamic_X = 0
	Dynamic_Y = 0
	ViralImpact = false
	MainMenu = true
	About = false
	TheEnd = false
	IntroActive = false
	
	Difficulty = 1
	Level = 1
	
	GameOver = false
	
	test = NameGen()
	
	datum = (math.random(23)..":"..math.random(59)..", "..math.random(1,28).."."..math.random(1,12).."."..math.random(2030,2035)..".")
	OwnPlace = CitySet[math.random(54)]
	
	-- sound effect
	SoundSet = { }
	
	SoundSet[1] = love.audio.newSource("nosign.wav", "static")
	SoundSet[2] = love.audio.newSource("nextlevel.wav", "static")
	SoundSet[3] = love.audio.newSource("logged.wav", "static")
	SoundSet[4] = love.audio.newSource("die.wav", "static")	
	SoundSet[5] = love.audio.newSource("terminal.wav", "static")
	SoundSet[6] = love.audio.newSource("change.wav", "static")	
	SoundSet[7] = love.audio.newSource("step.wav", "static")

	
	--uploading custom font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	
	
	--uploading some graphics I made
	TitlePic = love.graphics.newImage('LOGOFINAL.png')
	KatamoriBall = love.graphics.newImage('KATAMORI.png')
	Border_A = love.graphics.newImage('BORDER_VERT.png')
	Border_B = love.graphics.newImage('BORDER_HORI.png')
	Field = love.graphics.newImage('TEXTFIELD.png')	
	
	-- uploading tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*32, (i-1)*32, 32, 32, 96, 96)
		end
	end
	
	-- creating map 
	Map = { }
	
	for j=1,24 do
		
		Map[j] = { }
		
		for i=1,24 do
			Map[j][i] = 1
		end
	end
	
	--loading functions
	GenerateBrandNewLevel()
	
end

function love.update(dt)


	--mouse
	MouseX = love.mouse.getX()
	MouseY = love.mouse.getY()

	-- timing
	TimeChecker = TimeChecker+1	
	
	if not GameOver and not IntroActive and not MainMenu then
	IngameChecker = IngameChecker+1	
	Seconds = math.floor(IngameChecker/60)
	else
	IngameChecker = 0
	Seconds = 0
	end

	
	if not MainMenu then
		--counting field position
		MeOnField_X = math.floor((Own_X-512)/32)+1
		MeOnField_Y = math.floor(Own_Y/32)+1
		
		--calling functions that must be active in every frame
		if not GameOver and Map[MeOnField_X][MeOnField_Y] ~= 5 then
			ProcGen()
		end
		
		--behaviours that can be checked on every frame
		AntiVirus()
	end
end

function love.draw()

	if MainMenu then
	
		love.graphics.draw(TitlePic,512,800)
		love.graphics.draw(KatamoriBall,1080,800)	

	end
	
	if not MainMenu and not IntroActive then
		if not TheEnd then
			--HUD
			for k=0,8 do
				love.graphics.draw(Field,24,24+k*48)
			end
			
			for k=0,2 do
				love.graphics.draw(Field,24,632+k*48)
			end		
		end
	
		for j=1,24 do
			for i=1, 24 do
				love.graphics.drawq(TilesetPic, Tileset[Map[j][i]], 512+(j-1)*32, (i-1)*32)
				love.graphics.drawq(TilesetPic, Tileset[9], Own_X, Own_Y)
				end
			end
	end

	love.graphics.draw(Border_A,512,768)
	love.graphics.draw(Border_A,0,768)	
	love.graphics.draw(Border_B,480,0)	
	TextWriter()
	
end

function love.keyreleased(key)

	-- behaviour calling
	
	if key == 'w' or key == 's' or key == 'a' or key == 'd' then
		LogPoint_Logging()	
		VisitTerminal()
		
		if GameOver then
			Restart()
		end		
	end
	
	if key == ' ' then
		NextLevel()	
	end
	
	if not MainMenu then
	--moving
		if key == 'w' then
			if Own_Y > 0 and Physics(Map[MeOnField_X][MeOnField_Y-1]) then
			Own_Y = Own_Y - 32
			
			love.audio.play(SoundSet[7])
				if not SoundSet[7]:isStopped()
				then
					if TimeChecker%1==0 then
						SoundSet[7]:rewind()	
					end
				end	
			end
		end
			
		if key == 'a' then
			if Own_X > 512 and Physics(Map[MeOnField_X-1][MeOnField_Y]) then
			Own_X = Own_X - 32
			
			love.audio.play(SoundSet[7])
				if not SoundSet[7]:isStopped()
				then
					if TimeChecker%1==0 then
						SoundSet[7]:rewind()	
					end
				end	
			end
		end

		if key == 's' then
			if Own_Y < 736 and Physics(Map[MeOnField_X][MeOnField_Y+1]) then
			Own_Y = Own_Y + 32
			
			love.audio.play(SoundSet[7])
				if not SoundSet[7]:isStopped()
				then
					if TimeChecker%1==0 then
						SoundSet[7]:rewind()	
					end
				end	
			end
		end

		if key == 'd' then
			if Own_X < 1248 and Physics(Map[MeOnField_X+1][MeOnField_Y]) then
			Own_X = Own_X + 32
			
			love.audio.play(SoundSet[7])
				if not SoundSet[7]:isStopped()
				then
					if TimeChecker%1==0 then
						SoundSet[7]:rewind()	
					end
				end	
			end
		end	
	end
	-- just a simple exit, to make testing easier

	if key == 'escape' then
		if Level == 50 and not GameOver then
			TheEnd = true
		else
			love.event.push('quit')
		end
	end

end

function Restart()

--variable reset

	i,j,k,l,N = 0,0,0,0,0
	Own_X, Own_Y = 544,384
	MeOnField_X, MeOnField_Y = 1, 1

	TimeChecker = 0
	Seconds = 0
	RoundCounter = 0
	
	LoggedAmount = 0
	TerminalsVisited = 0
	TerminalCondition = 0
	HasDynamic = false
	DynamicDir = 1
	Dynamic_X = 0
	Dynamic_Y = 0
	ViralImpact = false
	MainMenu = true
	About = false
	TheEnd = false
	IntroActive = false	
	
	Difficulty = 1
	Level = 1
	
	GameOver = false

	--loading functions
	GenerateBrandNewLevel()
	
end
